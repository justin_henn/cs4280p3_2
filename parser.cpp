//parser
//This is the parser for make parse trees
//Justin Henn
//Assignment 2
////4/15/18
#include <string>
#include "scanner.h"
#include "token.h"
#include "node.h"
#include "tree.h"

using namespace std;

node_t* program();
node_t* vars();
node_t* block();
node_t* mvars();
node_t* stats();
node_t* stat();
node_t* mStat();
node_t* in();
node_t* out();
node_t* If();
node_t* loop();
node_t* assign();
node_t* expr();
node_t* RO();
node_t* M();
node_t* R();
void error(string s);

token tk;
FILE* file;  
int line_number = 1;

/*function to send chars to the scanner*/

token scanner() {

  char first_char;
  token output_token;

  first_char = fgetc(file);

  while (first_char == ' ' || first_char == '&' || first_char == '\n') {

    if (first_char == ' ')
      first_char = fgetc(file);
    if (first_char == '&') {

      while ((first_char = fgetc(file)) != '&') {

	;
      }
      first_char = fgetc(file);
    }
    if (first_char == '\n') {
    
      line_number++;
      first_char = fgetc(file);
    }
  }

  output_token = get_token(file, first_char, line_number);
  return output_token;

}

/*function for the parser*/

node_t* parser(node_t* root, string filename) {

  if ((file = fopen(filename.c_str(), "r")) == NULL) {

    printf("Cannot open file.\n");
    exit (0);
  }
  tk = scanner();
  root = program();
  if (tk.token_type == "EOFTk") {
  
    fclose(file);
    return root;
  }
  else
    error("expected eof");

}

/*function for program*/

node_t* program() {

  node_t* p = newNode("program");

  if (tk.instance == "program") {

    tk = scanner();
    p->left = vars();
    p->mid_left = block();
    return p;
  }
  else
    error("expected program");
}
node_t* block() {

  node_t* p = newNode("block");
  if (tk.token_type == "startTk") {

    tk = scanner();
    p->left = vars();
    p->mid_left = stats();
    if (tk.token_type == "stopTk") {

      tk = scanner();
      return p;
    }
    else
      error("expected stop");
  }
  else
    error("expected start");
}
node_t* vars() {
  
  node_t* p = newNode("vars");
  if (tk.token_type == "varTk") {

    tk = scanner();
 
    if(tk.token_type == "IDTk") {

      (p->different_tokens).push_back(tk);
      tk = scanner();
      if (tk.token_type == "=Tk") {
  
	tk = scanner();
 	if(tk.token_type == "digitTk") {
   
	  (p->different_tokens).push_back(tk);
	  tk = scanner();
	  p->left = mvars();
	  return p;
	}
	else
	  error("expected digit");
      } 
      else
	error("expected =");
     }
    else
      error("expected identifier");
    }
  else
    return p;
}

/*function for mvars*/

node_t* mvars() {
  
  node_t*p = newNode("mvars");
  if (tk.token_type == ".Tk") {

    tk = scanner();
    return p;
  }

  else if (tk.token_type == ":Tk") {

    tk = scanner();
    if (tk.token_type == "IDTk") {

      (p->different_tokens).push_back(tk);
      tk = scanner();
      p->left = mvars();
      return p;
  }
    else
      error("expected identifier");
  }
  else
    error("expected :");
}

/*function for expr*/

node_t* expr() {

  node_t* p = newNode("expr");
  p ->left = M();
  if (tk.token_type == "+Tk" || tk.token_type == "-Tk" || tk.token_type == "/Tk" || tk.token_type == "*Tk") {

    (p->different_tokens).push_back(tk);
    tk = scanner();
    p->mid_left = expr();
  }
  return p;
}

/*function for M*/

node_t* M() {

  node_t* p = newNode("M");
  if (tk.token_type == "%Tk") {

    tk = scanner();
    p->left = M();
    return p;
  }
  else {

    p->left = R();
    return p;
  }
}

/*function for R*/

node_t* R() {

  node_t* p = newNode("R");
  if (tk.token_type == "(Tk)") {

    tk = scanner();
    expr();
    if (tk.token_type == ")Tk") {

      tk = scanner();
      return p;
    }
    else
      error("expected )");
  }
  else if (tk.token_type == "IDTk" ) {

    (p->different_tokens).push_back(tk);
    tk = scanner();
    return p;
  }
  else if (tk.token_type == "digitTk") {

    (p->different_tokens).push_back(tk);
    tk = scanner();
    return p;
  }
  else
    error("expected ( or identifier or digit");
}

/*function for stats*/

node_t* stats() {

  node_t* p = newNode("stats");
  p->left = stat();
  p->mid_left = mStat();
  return p;
}
node_t* mStat() {

  node_t* p = newNode("mStat");
  if (tk.token_type == "startTk" || tk.token_type == "readTk" || tk.token_type == "printTk" || tk.token_type == "iffTk" || tk.token_type == "iterTk" || tk.token_type == "letTk") {

    p->left = stat();
    p->mid_left = mStat();
    return p;
  }
  else
    return p;
}

/*function for stat*/

node_t* stat() {

  node_t* p = newNode("stat");
  if (tk.token_type == "readTk"){

    tk = scanner();
    p->left = in();
    return p;
  }
  else if (tk.token_type == "printTk") {

    tk = scanner();
    p->left = out();
    return p;
  }
  else if (tk.token_type == "iffTk") {

    tk = scanner();
    p->left = If();
    return p;
  }
  else if (tk.token_type == "startTk") {

    p->left = block();
    return p;
  }
  else if (tk.token_type == "iterTk") {

    tk = scanner();
    p->left = loop();
    return p;
  }
  else if (tk.token_type == "letTk") {

    tk = scanner();
    p->left = assign();
    return p;
  }
  else
    error("expected read or print or iff or iter or let, or start");
}

/*function for in*/

node_t* in() {

  node_t* p = newNode("in");
  if (tk.token_type == "IDTk") {

    (p->different_tokens).push_back(tk);
    tk = scanner();
    if (tk.token_type == ".Tk") {

      tk = scanner();
      return p;
    }
    else
      error("expected .");
  }
  else
    error("expected identifier");
}

/*function for out*/

node_t* out () {

  node_t* p = newNode("out");
  p->left = expr();
  if (tk.token_type == ".Tk") {

    tk = scanner();
    return p;
  }
  else
    error("expected .");
}

/*function for if*/

node_t* If() {

  node_t* p = newNode("If");
  if (tk.token_type == "(Tk") {

    tk = scanner();
    p->left = expr();
    p->mid_left = RO();
    p->mid_right = expr();
    if (tk.token_type == ")Tk") {

      tk = scanner();
      p->right = stat();
      return p;
    }
    else
      error("expected )");
  }
  else
    error("expected (");
}

/*function for loop*/

node_t* loop() {

  node_t* p = newNode("loop");
  if (tk.token_type == "(Tk") {

    tk = scanner();
    p->left = expr();
    p->mid_left = RO();
    p->mid_right = expr();
    if (tk.token_type == ")Tk") {

      tk = scanner();
      p->right = stat();
      return p;
    }
    else
      error("expected )");
  }
  else
    error("expected (");
}

/*function for assign*/

node_t* assign() {

  node_t* p = newNode("assign");
  if (tk.token_type == "IDTk") {

    (p->different_tokens).push_back(tk);
    tk = scanner();
    if (tk.token_type == "=Tk") {

      tk = scanner();
      p->left = expr();
      if (tk.token_type == ".Tk") {

	tk = scanner();
	return p;
      }
      else
	error("expected .");
    }
    else
      error("expected =");
  }
  else
    error("expected identifier");
}

/*function for RO*/

node_t* RO() {

  node_t* p = newNode("RO");
  if (tk.token_type == "<Tk") {

    (p->different_tokens).push_back(tk);
    tk = scanner();
    if (tk.token_type == "<Tk") {

      (p->different_tokens).push_back(tk);
      tk = scanner();
      return p;
    }
    else
      return p;
  }
  else if (tk.token_type == ">Tk") {

    (p->different_tokens).push_back(tk);
    tk = scanner();
    if (tk.token_type == ">Tk") {

      (p->different_tokens).push_back(tk);
      tk = scanner();
      return p;
    }
    else
      return p;
  }
  else if (tk.token_type == "=Tk") {

    (p->different_tokens).push_back(tk);
    tk = scanner();
    if (tk.token_type == "=Tk") {

      (p->different_tokens).push_back(tk);
      tk = scanner();
      return p;
    }
    else
      return p;
  }
  else
    error("expected < or > or =");
}

/*function for errors*/

void error(string s) {

  cout << tk.instance <<  " Recieved " << tk.token_type << " at line number: " << tk.line_number << ". " << s <<"\n";
  fclose(file);
  exit(1);
}

