//static_semantics.cpp
//This is the file for running the static semantics
//Justin Henn
//Assignment 3
//4/22/18

#include <stdlib.h>
#include <iostream>
#include <string>
#include <string.h>
#include <vector>
#include "stack_node.h"
#include "stack.h"
#include "node.h"

using namespace std;

/*function for starting the semantics check */

void static_semantics(node_t* root, int varCount);

stack_node* front;

void start_semantics(node_t* root) {

    init(front);
    static_semantics(root, 0);

}

/*function that does that static semantics checking*/

void static_semantics(node_t* root, int varCount) {


  node_t* temp = root;
  if(temp == NULL) {

    return;
  }

  if (temp->key == "block"){
    varCount = 0;

  }

  int stack_check = 0;
  if (!(temp->different_tokens).empty()) {

    if (temp->key == "vars" || temp->key == "mvars") {

      if (varCount > 0) {

	stack_check = find(front, (temp->different_tokens)[0].instance);
	if (stack_check < varCount && stack_check > -1) {

	  cout << "Error in " << temp->key << " " << (temp->different_tokens)[0].instance << "\n" ;
	  exit (1);
	}		
      }
      front = push(front, (temp->different_tokens)[0].instance);
      varCount++;
    }
    else if (!isalpha((temp->different_tokens)[0].instance[0])) {
        
    }
    
    else {

      stack_check = find(front, (temp->different_tokens)[0].instance);
      if (stack_check == -1) {

	cout << "Error in " << temp->key << " " << (temp->different_tokens)[0].instance << "\n";
	exit (1);
      }
    }
  }
  static_semantics(temp->left, varCount);
  static_semantics(temp->mid_left, varCount);
  static_semantics(temp->mid_right, varCount);
  static_semantics(temp->right, varCount);
  if(temp->key == "block") {

    for (int x = 0; x < varCount; x++)
      front = pop(front);
  }
}
