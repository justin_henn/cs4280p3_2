//static_semantics.h
//This is the static stemantics h file
//Justin Henn
//Assignment 3
//4/22/18

#ifndef STATIC_SEM_H
#define STATIC_SEM_H
#include <string>
#include "stack_node.h"
#include "node.h"

using namespace std;

void start_semantics(node_t* root);
#endif
