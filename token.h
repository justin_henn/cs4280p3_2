//token.h
////This is the h file that holds the token struct
//Justin Henn
//Assignment 2
//4/2/18
#ifndef TOKEN_H
#define TOKEN_H
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <string>

using namespace std;

typedef struct {

  string token_type;
  string instance;
  int line_number;

} token;

#endif
